[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3236335.svg)](https://doi.org/10.5281/zenodo.3236335)

# Tripal MegaSearch
Tripal MegaSearch is a tool for downloading biological data stored in a Tripal/Chado database. Site administrators may choose from two different data sources to serve data, i.e. Tripal MegaSearch materialized views (MViews) or Chado base tables. If neither data source is desired, administrators may also create their own MViews and serve them through Tripal MegaSearch. Site administrators can also set download limits according to their server's capability.

Tripal MegaSearch implements a flexible dynamic query form on which filters can be added dynamically. These filters are prepopulated with values mapped to the underlying data source columns so the user can filter data on each column. The user can add as many filters as s/he desires and combine them using 'AND/OR' operators.

More complex static forms are also available if the Tripal MegaSearch Mviews are used as the data source. You can adjust these MViews according to the way the data is stored in your database and populate them with the modified SQL statement. Doing so allows you to take advantages of the complex static query forms when serving data.

Tripal MegaSearch is created by Main Bioinformatics Lab (Main Lab) at Washington State University. Information about the Main Lab can be found at: https://www.bioinfo.wsu.edu

## Screenshot
![Image](screenshot.jpg "screenshot")

## Requirement
 - Drupal 7.x
 - Tripal 7.x-2.x or Tripal 7.x-3.x
 - Chado Search v2.1.0 or the GitLab master branch (https://gitlab.com/mainlabwsu/chado_search)

## Version
1.2.1

## Download
The Tripal MegaSearch module can be downloaded from GitLab:

https://gitlab.com/mainlabwsu/tripal_megasearch

## Installation

1. Download the dependency 'chado_search' module (master branch) into Drupal's module directory:

    ```
    cd [DRUPAL_ROOT]/sites/all/modules
    git clone https://gitlab.com/mainlabwsu/chado_search
    ```

2. Download 'tripal_megasearch':

    ```
    git clone https://gitlab.com/mainlabwsu/tripal_megasearch
    ```

3. Create an empty conf file for 'chado_search' to suppress a warning about conf file not found if Chado Search is set up the first time:

    ```
    cd [DRUPAL_ROOT]/sites/all/modules/chado_search/file
    touch settings.conf
    ```

4. Enable both 'chado_search' and 'tripal_megasearch' modules. This will create a set of MViews with prefix names of 'tripal_megasearch_':

    Go to: Administration > Modules, check Mainlab Chado Search and Tripal MegaSearch (under the Mainlab category) and save

    or by using the 'drush' command:

    ```
    drush pm-enable chado_search tripal_megasearch
    ```

5. Populate 'tripal_megasearch' MViews using the Tripal interface if you wish to use tripal_megasearch MViews as data source. Ignore this step if you want to use Chado base tables as data source or plan to create your own MViews:

     Go to: Tripal > Chado Schema > Materialized Views (or Tripal v3:  Tripal > Data Storage > Chado > Materialized Views)

     Identify materialized views with 'tripal_megasearch_' prefix. Click on 'Populate' to submit a Tripal job for all of them.

     Launch the job from the console. This can usually be done by switching to the web root directory and issue the drush command:

     ```
     drush trp-run-jobs --username=<an admin user>
     ```

      Note: More information about using the Tripal Materialized Views system can be found at
           http://tripal.info/node/105

6. To change the settings for tripal_megasearch, use the URL below. Here you can change the data source, query form, and download limits, etc.:

    Go to: https://your.site/admin/mainlab/tripal_megasearch

7. To start using tripal_megasearch:

    Go to: https://your.site/tripal_megasearch

## Note
  - Please note that Tripal MegaSearch uses session. If the form becomes stale when user login or logout the website (i.e. session destroyed), the query form will need to be reset. This can be achieved by clicking the 'Reset' button on the form or by revisiting the Tripal MegaSearch URL from the browser.

  - Tripal MegaSearch uses Chado Search API and creates downloadable files in '[DRUPAL_ROOT]/sites/default/files/tripal/chado_search' directory. Site administrators may wish to clean this directory from time to time to release the space. Do not delete this direcotry or Tripal MegaSearch will not be able to create download files. Make sure the webserver (e.g. www-data) has write permission to this directory if you recreate it manually.

## Administration (Admin > mainlab > tripal_megasearch)

  The following are the options you can change from the administrative interface:

 - Data Source:
   The file that defines all downloadable data as tables or materialized views (MView) in a function named 'tripal_megasearch_data_definition' which returns a PHP associative array. This file should be in the 'conf' sub-directory and have an extension of .inc. The definition will be used for creating dynamic form filters and downloadable fields. You can create your own data source file that meets above requirements which will then show up here as an option.

 - Query Form:
   There are two types of query forms. The dynamic form allows users to add filters dynamically according to the columns of a table or MView defined in the data source. The static forms are pre-defined forms that work best with the MViews preinstalled by Tripal MegaSearch. However, they may not suit everyone if data are stored in different ways. Note: If static forms are not defined, Tripal MegaSearch will fall back to use the dynamic form.

 - Dynamic Form Autocomplete:
    Turn autocomplete on or off for the dynamic form. If this is on, the filters will show matching values once the user starts typing.

 - Download Limit:
   The download limit on each query. Increase this number may add to the server load. Set to 0 for unlimited download. (Default = 200000)

 - FASTA Download Limit:
   The limit for downloading FASTA records on each query. Increase this number may add to the server load. Set to 0 for unlimited FASTA records. (Default = 50000)

 - Form Instruction:
   The instruction to show on the query form. Token %limits% can be used to show the download limits set above.

 - Result Page:
   When a user clicks on the 'View' result button, remove duplicates from the results (can be very slow for large result set due to the amount of data needed to be processed). Default is not to remove duplicates. To configure this for individual dataset, add a 'duplicates' key in the data definition conf file to override this global setting. (allowed either 'remove' or 'not_remove' value).

 - Number of rows per result page:
   The number of rows to show on each page when viewing the results. If the value is less than 1, the 'View' result button will be disabled. (Default=10)

## Customization
 - **Changing Data Source**

    You can customize Tripal MegaSearch to include any table or materialized view and serve it to the users with a dynamic form. Tripal MegaSearch work practically with any table (or mview). Here are the steps:
    1. Create a data definition file in the [MODULE_ROOT]/conf directory. Please note that the file needs to be in the 'conf' directory and its name has to end with '.inc' You can copy the Tripal MegaSearch MView (i.e. tripal_megasearch.mviews.inc) or Chado Base Tables (i.e. chado.base_tables.inc) definition file if you want to modify it.
    ```
    cd [MODULE_ROOT]/conf
    touch my_data.inc
     ```

    2.  Edit the data definition file. The file should contain a function named 'tripal_megasearch_data_definition' and returns an associative array with the settings described in the next step.
    ```
    function tripal_megasearch_data_definition () {
      $def = array();
      return $def;
    }
    ```

    3. The returned associated array should be structured as follow:

    ```
    $def[$table] = array (                          // $table should match the table name in the database
      'name' => 'dataset_name',                     // The name to show in the Data Type dropdown
      'field' => array (                            // Fields to show in both Dynamic Form Filter dropdown and Downloadable Fields checkboxes
        $column1 => 'label1',                       // $column should match the column in the table
        $column2 => 'label2',
      )
      'field_link_callback' => array (                                // (Optional) Hyperlink callback function that returns a URL for fields
        $column1 => '$callback:$column1,$column2,...'  // $column should match the column in the table. $callback is a valid PHP callback that returns a URL for link-out. $column1, $column2, etc will be passed in to the callback as arguments
        $column2 => '$callback:$column1,$column2,...',
        ...
      )
      'form' => 'static_form_callback_function',    // (Optional) Callback for the Static Form. If this is not provided or callback does not exist, fall back to use the Dynamic Form
      'file' => 'custom_dir/custom_file.inc'        // (Optional) Include specified file when searching for the callback. Accept only relative path from the module root directory. Create custom sub-directory accordingly.
      'fasta' => 'column_to_provide_fasta_download' // (Optional) Provide a FASTA download using specified column (usually feature_id)
      'count' => 'column_to_count_unique_records'   // (Optional) Column for counting unique records (e.g. stock_id)
    )
    ```

     4. Go to the Tripal MegaSearch administrative page (https://your.site/admin/mainlab/tripal_megasearch) and switch the data source to use the custom file (i.e. my_data.inc)

 - **Modifying query forms**

    If you want to disable or modify any form, you can make a copy of the Data Source conf file and edit it accordingly. The following example shows you how to remove 'Country' and 'Image' filters from the static Germplasm search form.
    1. Make a copy of the 'tripal_mesasearch.mviews.inc' so we can modify it and use it as the new data source fiile (e. g. my_data.inc).
    ```
    cd [MODULE_ROOT]/conf
    cp tripal_mesasearch.mviews.inc my_data.inc
     ```
     2.  Edit my_data.inc and comment out the fields you don't want to show as 'Downloadable Fields' on the form with //:
   ```
    function tripal_megasearch_stock ($def = array()) {
	  $def['tripal_megasearch_stock'] = array(
	    'name' => 'Germplasm',
	    'field' => array(
	      'name' => 'Name',
	      'uniquename' => 'Unique Name',
	      'accession' => 'Accession',
	      'maternal_parent' => 'Maternal Parent',
	      'paternal_parent' => 'Paternal Parent',
	      'pedigree' => 'Pedigree',
	  //   'country' => 'Country',
	  //    'eimage_data' => 'Image Name',
	  //    'legend' => 'Legend',
	    ),
	    'form' => 'tripal_megasearch_stock_form',
	    'file' => 'includes/tripal_megasearch.form.static.stock.inc',
	    'count' => 'stock_id'
	  );
	  return $def;
	}
    ```
    3. Edit my_data.inc and change the form callback to use the version in a cutom file (i.e. 'custom/my.form.static.stock.inc')
    ```
      // 'file' => 'includes/tripal_megasearch.form.static.stock.inc',
      'file' => 'custom/my.form.static.stock.inc',
    ```

    4. Create the custom directory and file:
    ```
    cd [MODULE_ROOT]
    mkdir custom
    cp includes/tripal_megasearch.form.static.stock.inc custom/my.form.static.stock.inc
    ```

    5. Comment out or delete the form elements you don't want in the 'custom/my.form.static.stock.inc' file. In this case 'Country' and Image' filters
    ```
	Line169     // Country
				      //$form->addSelectFilter(
				      //    Set::selectFilter()
				      //    ->id('stock_country')
				      //    ->title('Country')
				      //    ->table('tripal_megasearch_stock')
				      //    ->column('country')
				      //    ->labelWidth(140)
				      //    ->fieldset('data_filters')
				      //   ->cache(TRUE)
				      //   ->newline()
				      //   );
				      //$form->addFieldset(
				      //    Set::fieldset()
				      //    ->id('stock_group_country')
				      //    ->title('Country')
				      //    ->startWidget('stock_country')
				      //    ->endWidget('stock_country')
				      //    );
				      //
				      // Image
				      //$form->addTextFilter(
				      //    Set::textFilter()
				      //    ->id('stock_eimage_data')
				      //    ->title('Name')
				      //    ->labelWidth(140)
				      //    ->fieldset('data_filters')
				      //    ->newline()
				      //    );
				      //$form->addTextFilter(
				      //    Set::textFilter()
				      //    ->id('stock_legend')
				      //    ->title('Legend')
				      //    ->labelWidth(140)
				      //    ->fieldset('data_filters')
				      //    ->newline()
				      //    );
				      //$form->addFieldset(
				      //    Set::fieldset()
				      //    ->id('stock_group_image')
				      //    ->title('Image')
				      //    ->startWidget('stock_eimage_data')
				      //    ->endWidget('stock_legend')
	Line212      //);
    ```

    6. Edit 'custom/my.form.static.stock.inc' file.  Comment out or remove those filters in the 'tripal_megasearch_ajax_stock_get_filters' function so they won't be used when submitting the form
    ```
	  function tripal_megasearch_ajax_stock_get_filters(&$form_state) {
		  $where = array();
		  $where [] = Sql::selectFilter('stock_organism', $form_state, 'organism');
		  $where [] = Sql::textFilter('stock_name', $form_state, 'name');
		  $where [] = Sql::file('stock_file', 'name', FALSE, TRUE);
		  $where [] = Sql::selectFilter('stock_collection', $form_state, 'collection');
		  $where [] = Sql::textFilter('stock_accession', $form_state, 'accession');
		  $where [] = Sql::textFilter('stock_maternal_parent', $form_state, 'maternal_parent');
		  $where [] = Sql::textFilter('stock_paternal_parent', $form_state, 'paternal_parent');
		  $where [] = Sql::textFilter('stock_pedigree', $form_state, 'pedigree');
		// $where [] = Sql::selectFilter('stock_country', $form_state, 'country');
		// $where [] = Sql::textFilter('stock_eimage_data', $form_state, 'eimage_data');
		//  $where [] = Sql::textFilter('stock_legend', $form_state, 'legend');
		  return $where;
      }
    ```

    7. Go to the Tripal MegaSearch administrative page (https://your.site/admin/mainlab/tripal_megasearch) and switch the data source to use the custom file (i.e. my_data.inc). Since this is a static form, you'll need to change the 'Query Form' setting to 'Static Form' in the admin interface as well. If you want to use dynamic form for other data types, just remove the 'form' and 'file' keys from 'my_data.inc'. Tripal MegaSearch will fall back to use dynamic form if a static form callback can not be found.

        Note: all of the query forms (including dynamic and static ones) are created using the Chado Seach Form API.

## Problems/Suggestions
Tripal MegaSearch module is still under active development. For questions or bug
report, please contact the developers at the Main Bioinformatics Lab by emailing to:
dev@bioinfo.wsu.edu

## Citation
If you use Tripal MegaSearch, please cite:

[Jung S, Cheng CH, Buble K, Lee T, Humann J, Yu J, Crabb J, Hough H, Main D. 2021. Tripal MegaSearch: a tool for interactive and
customizable query and download of big data. Database. 10.1093/database/baab023](https://academic.oup.com/database/article/doi/10.1093/database/baab023/6253732)