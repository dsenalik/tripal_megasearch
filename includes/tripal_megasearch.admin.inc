<?php

function tripal_megasearch_admin_form($form, &$form_state) {
  $form = array();
  
  $default_mview = variable_get('tripal_megasearch_data_file', 'tripal_megasearch.mviews.inc');
  $dir = drupal_get_path('module', 'tripal_megasearch') . '/conf';
  $files = scandir($dir);
  $options = array();
  foreach ($files AS $f) {
    if ($f != '.' && $f != '..') {
      if (preg_match('/\.inc$/',$f)) {
        $options [$f] = $f;
      }
    }
  }
  $form['data_file'] = array(
    '#type' => 'radios',
    '#title' => 'Data Source',
    '#description' => 'The file that defines all downloadable data as tables or materialized views (MView) in a function named \'tripal_megasearch_data_definition\' which returns a PHP associative array. This file should be in the \'conf\' sub-directory and have an extension of .inc. The definition will be used for creating dynamic form filters and downloadable fields. You can create your own data source file that meets above requirements which will then show up here as an option.',
    '#options' => $options,
    '#default_value' => $default_mview
  );
  $default_query_form = variable_get('tripal_megasearch_query_form', 'dynamic');
  $form['query_form'] = array(
    '#type' => 'radios',
    '#title' => 'Query Form',
    '#description' => 'There are two types of query forms. The dynamic form allows users to add filters dynamically according to the columns of a table or MView defined in the data source. The static forms are pre-defined forms that work best with the MViews preinstalled by Tripal MegaSearch. However, they may not suit everyone if data are stored in different ways. Note: If static forms are not defined, Tripal MegaSearch will fall back to use the dynamic form.',
    '#options' => array('dynamic' => 'Dynamic Form', 'static' => 'Static Form'),
    '#default_value' => $default_query_form
  );
  $default_autocomplete = variable_get('tripal_megasearch_dynamic_form_autocomplete', 'off');
  $form['dynamic_form_autocomplete'] = array(
    '#type' => 'radios',
    '#title' => 'Dynamic Form Autocomplete',
    '#description' => 'Turn autocomplete on or off for the dynamic form. If this is on, the filters will show matching values once the user starts typing.',
    '#options' => array('on' => 'On', 'off' => 'Off'),
    '#default_value' => $default_autocomplete
  );
  $default_link = variable_get('tripal_megasearch_link_in_new_tab', 'off');
  $form['link_in_new_tab'] = array(
      '#type' => 'radios',
      '#title' => 'Open links in new tab',
      '#description' => 'Open links in a new browser tab instead of the current tab.',
      '#options' => array('on' => 'On', 'off' => 'Off'),
      '#default_value' => $default_link
  );
  $default_limit = variable_get('tripal_megasearch_download_limit', '200000');
  $form['download_limit'] = array(
    '#type' => 'textfield',
    '#title' => 'Download Limit',
    '#description' => 'The download limit on each query. Increase this number may add to the server load. Set to 0 for unlimited download. (Default = 200000)',
    '#default_value' => $default_limit
  );
  $default_fasta_limit = variable_get('tripal_megasearch_fasta_download_limit', '50000');
  $form['fasta_download_limit'] = array(
    '#type' => 'textfield',
    '#title' => 'FASTA Download Limit',
    '#description' => 'The limit for downloading FASTA records on each query. Increase this number may add to the server load.  Set to 0 for unlimited FASTA records. (Default = 50000)',
    '#default_value' => $default_fasta_limit
  );
  
  $default_instruction = variable_get('tripal_megasearch_instruction', 'Tripal MegaSearch is a tool for downloading biological data. %limits%<br><br>Select a data type to start building your own query and download data in bulk:');
  $form['instruction'] = array(
    '#type' => 'textfield',
    '#title' => 'Form Instruction',
    '#description' => 'The instruction to show on the query form. Token %limits% can be used to show the download limits set above.',
    '#default_value' => $default_instruction,
    '#maxlength' => 1280,
    '#size' => 140
  );
  
  $default_duplicates = variable_get('tripal_megasearch_result_duplicates', 'not_remove');
  $form['result_duplicates'] = array(
    '#type' => 'radios',
    '#title' => 'Result Page',
    '#description' => 'Remove duplicates from the results (can be very slow for large result set due to the amount of data needed to be processed). Default is not to remove duplicates. To configure this for individual dataset, add a \'duplicates\' key in the data definition conf file to override this global setting. (allowed either \'remove\' or \'not_remove\' value) ',
    '#options' => array('not_remove' => 'Do not remove duplicates (Fast)', 'remove' => 'Remove duplicates (Slow. Number of results changes according to the selected downloable fields)'),
    '#default_value' => $default_duplicates
  );
  
  $default_rows = variable_get('tripal_megasearch_result_rows', '10');
  $form['result_rows'] = array(
    '#type' => 'textfield',
    '#title' => 'Number of rows per result page',
    '#description' => 'The number of rows to show on each page when viewing the results. If the value is less than 1, the \'View\' result button will be disabled. (Default=10)',
    '#default_value' => $default_rows,
    '#maxlength' => 3,
    '#size' => 10
  );

  return system_settings_form($form);
}

function tripal_megasearch_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  variable_set('tripal_megasearch_query_form', $values['query_form']);
  variable_set('tripal_megasearch_dynamic_form_autocomplete', $values['dynamic_form_autocomplete']);
  variable_set('tripal_megasearch_link_in_new_tab', $values['link_in_new_tab']);
  variable_set('tripal_megasearch_instruction', $values['instruction']);
  variable_set('tripal_megasearch_data_file', $values['data_file']);
  if (is_numeric($values['download_limit'])) {
    variable_set('tripal_megasearch_download_limit', (int) $values['download_limit']);
  }
  if (is_numeric($values['fasta_download_limit'])) {
    variable_set('tripal_megasearch_fasta_download_limit', (int) $values['fasta_download_limit']);
  }
  variable_set('tripal_megasearch_result_duplicates', $values['result_duplicates']);
  if (is_numeric($values['result_rows'])) {
    variable_set('tripal_megasearch_result_rows', (int) $values['result_rows']);
  }
  menu_rebuild();
}