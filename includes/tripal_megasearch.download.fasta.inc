<?php 

use ChadoSearch\SessionVar;
use ChadoSearch\Sql;
use ChadoSearch\result\Fasta;

/*
 * Create FASTA download
 */
function tripal_megasearch_ajax_create_fasta_download ($form, &$form_state) {
  $where = array();
  $where [] = Sql::repeatableText('conditions', $form_state);
  return tripal_megasearch_ajax_get_fasta_file ($form, $form_state, $where);
}

function tripal_megasearch_ajax_get_fasta_file ($form, &$form_state, $conditions = array()) {
  $limit = $form_state['storage']['fasta_limit'];
  $status = $form['status'];
  $values = $form_state['values'];
  $mview = $values['datatype'];
  $where = '';
  // Remove NULL filters
  $valid_cond = array();
  foreach ($conditions AS $cond) {
    if (trim($cond)) {
      $valid_cond [] = $cond;
    }
  }
  $num_cond = count($valid_cond);
  if ($num_cond > 0) {
    $where = ' WHERE ';
  }
  $index = 0;
  foreach ($valid_cond AS $cond) {
    $where .= $cond . ' ';
    if ($index < $num_cond - 1) {
      $where .= ' AND ';
    }
    $index ++;
  }
  $sql ="SELECT * FROM {" . $mview . "}" . $where;
  SessionVar::setSessionVar('tripal_megasearch','sql', $sql);
  $sql_total = "SELECT count(*) FROM {" . $mview . "}";
  // if we only want to count unique value of a certain column:
  $count_col = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'count');
  if ($count_col) {
    $sql_total = "SELECT count(DISTINCT $count_col) FROM {" . $mview . "}";
  }
  $total = chado_search_query($sql_total . $where)->fetchField();
  if ($total == 0) {
    $status['#markup'] = '<font class="tripal_megasearch-error" color="red">No result. Please adjust the query and try again.</font>' . '<script>jQuery(\'.chado_search-tripal_megasearch-waiting-box\').hide();</script>';
    return $status;
  }
  else if ($limit && $total > $limit) {
    $status['#markup'] = '<strong>' . number_format($total) . '</strong> records. ' . '<font class="tripal_megasearch-error" color="red">The results exceed the limit of ' . number_format($limit) . ' FASTA sequences. Please add some filters and try again. </font>' . '<script>jQuery(\'.chado_search-tripal_megasearch-waiting-box\').hide();</script>';
    return $status;
  }
  else {
    SessionVar::setSessionVar('tripal_megasearch','total-items', $total);
  }
  $dl = new Fasta('tripal_megasearch', 'tripal_megasearch');
  $results = $dl->createFasta();
  $name = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'name');
  $message = '<strong>' . number_format($total) . ' ' . $name . '</strong>.  <i>Note: actual rows in downloaded file depend on the selected fields.<i>';
  $status['#markup'] = $message . '<script>jQuery(\'.chado_search-tripal_megasearch-waiting-box\').hide();</script>' . '<script type="text/javascript">window.location = "' . $results['path'] . '"</script>';
  return $status;
}