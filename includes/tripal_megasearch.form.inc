<?php

require_once 'tripal_megasearch.download.inc';
require_once 'tripal_megasearch.download.fasta.inc';
require_once 'tripal_megasearch.table.inc';

use ChadoSearch\Set;
use ChadoSearch\SessionVar;
use ChadoSearch\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function tripal_megasearch_query_build_form ($form) {

  $form_state =& $form->form_state;
  $form_state['storage']['limit'] = variable_get('tripal_megasearch_download_limit', '200000');
  $form_state['storage']['fasta_limit'] = variable_get('tripal_megasearch_fasta_download_limit', '50000');
  
  // Description
  $desc = t(variable_get('tripal_megasearch_instruction', 'Tripal MegaSearch is a tool for downloading biological data. %limits%<br><br>Select a data type to start building your own query and download data in bulk:'));
  $desc_dl = '';
  $desc_fa = '';
  if ($form_state['storage']['limit']) {
    $desc_dl = number_format($form_state['storage']['limit']) . ' records.';
  }
  if ($form_state['storage']['fasta_limit']) {
    $desc_fa = number_format($form_state['storage']['fasta_limit']) . ' FASTA sequences.';
  }
  if ($desc_dl || $desc_fa) {
    $desc = str_replace('%limits%', '(Current limit per download: ' . $desc_dl . ' ' . $desc_fa . ')', $desc);
  }
  else {
    $desc = str_replace('%limits%','', $desc);
  }
  $form->addMarkup(
      Set::markup()
      ->id('description')
      ->text($desc)
      ->newLine()
   );
  
  // Data Type
  $options = chado_search_get_all_mview_settings('tripal_megasearch_data_definition', 'name');
  asort($options);
  $form->addSelectOptionFilter(
      Set::selectOptionFilter()
      ->id('datatype')
      ->title('Data Type')
      ->options($options)
      ->noKeyConversion(TRUE)
  );
  // Reset Button
  $form->addReset(
      Set::reset()
      ->id('tripal_megasearch_reset')
      ->newLine());
  
  // Status & Counter
  $form->addDynamicMarkup(
    Set::dynamicMarkup()
      ->id('status')
      ->dependOnId('datatype')
      ->callback('tripal_megasearch_ajax_get_message')
      ->newLine()
  );
  
  // Dynamic Form
  $query_form = variable_get('tripal_megasearch_query_form', 'dynamic');
  if ($query_form == 'dynamic') {
    module_load_include('inc', 'tripal_megasearch', 'includes/tripal_megasearch.form.dynamic');
    $form = tripal_megasearch_query_form_dynamic($form);
    
  // Static Forms (Predefined)
  } else if ($query_form == 'static') {
    module_load_include('inc', 'tripal_megasearch', 'includes/tripal_megasearch.form.static');
    $form = tripal_megasearch_query_form_static($form);
  }
  
  $form->addMarkup(
      Set::markup()
      ->id('results')
      );
  $form->form['datatype']['#attribute']['update']['results']['wrapper'] = 'chado_search-tripal_megasearch-markup-results';
  return $form;
}

/*
 *  Return updated status message
 */
function tripal_megasearch_ajax_get_message($mview, $form, $form_state, $conditions = array()) {
  $message = NULL;
  if ($mview != '0') {
    $total = 0;
    $applyfilters = isset($form_state['triggering_element']['#id']) && $form_state['triggering_element']['#id'] == 'chado_search-id-apply_filters';
    if (chado_table_exists($mview) || db_table_exists($mview)) {
      $sql_total = "SELECT count(*) FROM {" . $mview . "}";
      // if we only want to count unique value of a certain column:
      $count_col = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'count');
      if ($count_col) {        
        $sql_total = "SELECT count(DISTINCT $count_col) FROM {" . $mview . "}";
      }
      // apply filters
      $where = '';
      if ($applyfilters) {
        // Remove NULL filters
        $valid_cond = array();
        foreach ($conditions AS $cond) {
          if (trim($cond)) {
            $valid_cond [] = $cond;
          }
        }
        $num_cond = count($valid_cond);
        if ($num_cond > 0) {
          $where = ' WHERE ';
        }
        $index = 0;
        foreach ($valid_cond AS $cond) {
          $where .= $cond . ' ';
          if ($index < $num_cond - 1) {
            $where .= ' AND ';
          }
          $index ++;
        }
      }

      // Run SQL
      // Apply filters and get count ONLY IF there is some filter applied
      if ($where) {
        $total = chado_search_query($sql_total . $where)->fetchField();
      }
      // Get count from cache
      else {
        $total = tripal_megasearch_get_total_from_cache($mview, $sql_total); 
      }
      SessionVar::setSessionVar('tripal_megasearch','total-items', $total);
    }
    $name = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'name');
    $message = '<strong>' . number_format($total) . ' ' . $name . '</strong>.  <i>Note: actual rows in downloaded file depend on the selected fields.<i>';
  }
  return $message;
}

/**
 * Get total record count from cache if there is no filter applied
 */
function tripal_megasearch_get_total_from_cache($mview, $sql_total) {
  $mview_time = 
  db_select('tripal_mviews', 'tm')
  ->fields('tm', array('last_update'))
  ->condition('name', $mview)
  ->execute()
  ->fetchField();
  $stats = variable_get('tripal_megasearch_mview_count-' . $mview, array());
  $total = 0;
  if (count($stats) > 0) {
    if ($mview_time == $stats['updated']) {
      return $stats['total'];
    }
  }
  $total = chado_search_query($sql_total)->fetchField();
  if ($mview_time) {
    $data = array('updated' => $mview_time, 'total' => $total);
    variable_set('tripal_megasearch_mview_count-' . $mview, $data);
  }
  return $total;
}

/**
 * Return $form['status'] element with updated message 
 */

function tripal_megasearch_get_form_status ($form, &$form_state, $where) {
  $status = $form['status'];
  $mview = $form_state['values']['datatype'];
  $message = tripal_megasearch_ajax_get_message($mview, $form, $form_state, $where);
  $status['#markup'] = $message . '<script>jQuery(\'.chado_search-tripal_megasearch-waiting-box\').hide();</script>';
  return $status;
}

function tripal_megasearch_ajax_filter_clear_values ($form, &$form_state) {
    return $form['data_filters'];
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_refresh_count ($form, &$form_state) {
  $where = array();
  $where [] = Sql::repeatableText('conditions', $form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_view_result ($form, &$form_state) {
  $where = array();
  $where [] = Sql::repeatableText('conditions', $form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/**
  Toggle select/unselect all fields
  **/
function tripal_megasearch_ajax_toggle_field_selection ($form, &$form_state) {
  $checked = $form_state['values']['attribute_check_all']['select_all'];  
  $attributes = $form['data_attributes']['attribute_checkboxes'];
  if($checked) {
    foreach ($attributes AS $k => $v) {
      if (!preg_match('/^#.+$/', $k)) {
        $attributes[$k]['#checked'] = TRUE;
      }
    }
  }
  else {
    foreach ($attributes AS $k => $v) {
      if (!preg_match('/^#.+$/', $k)) {
        $attributes[$k]['#checked'] = FALSE;
      }
    }
  }  
  return $attributes;
}
