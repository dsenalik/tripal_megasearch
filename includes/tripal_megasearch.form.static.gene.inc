<?php

use ChadoSearch\Set;
use ChadoSearch\Sql;

function tripal_megasearch_gene_form ($form) {
  $form_state =& $form->form_state;
  
  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
  Set::dynamicFieldset()
  ->id('data_filters')
  ->title('Query')
  ->dependOnId('datatype');
  $setttings_attr =
  Set::dynamicFieldset()
  ->id('data_attributes')
  ->title('Downloadable Fields')
  ->dependOnId('datatype');
  if(!isset($form_state['values']['datatype']) || $form_state['values']['datatype'] == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }
  
  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);
  
  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);
  
  if(isset($form_state['values']['datatype'])) {
    $mview = $form_state['values']['datatype'];
    $fields = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
    if ($fields) {
      $form->addButton(
          Set::button()
          ->id('tsv-download')
          ->value('TSV')
          ->fieldset('data_attributes')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_gene_create_download',
              'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
              'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $form->addSubmit(
          Set::submit()
          ->value('CSV')
          ->fieldset('data_attributes')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_gene_create_download',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      // Fasta download if available
      $fasta = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'fasta');
      if ($fasta) {
        $form->addButton(
            Set::button()
            ->id('fasta_file')
            ->fieldset('data_attributes')
            ->value('FASTA')
            ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_gene_create_fasta_download',
              'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
              'effect' => 'fade')
                )
            ->attributes(tripal_megasearch_generate_progress_attribute())
            );
      }
      $default_rows = variable_get('tripal_megasearch_result_rows', '10');
      if ($default_rows > 0) {
      $form->addButton(
          Set::button()
          ->id('view_results')
          ->fieldset('data_attributes')
          ->value('View')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_gene_view_result',
            'wrapper' => 'chado_search-tripal_megasearch-markup-results',
            'effect' => 'fade')
              )
          );
      }
      $form->addButton(
          Set::button()
          ->id('apply_filters')
          ->fieldset('data_filters')
          ->value('Refresh Count')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_gene_refresh_count',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_waiting_attribute())
          );
      $form->addButton(
          Set::button()
          ->id('clear-all-values')
          ->fieldset('data_filters')
          ->value('Clear')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_filter_clear_values',
              'wrapper' => 'chado_search-filter-tripal_megasearch-data_filters-field',
              'effect' => 'fade')
              )
          );
      // Sequence Type
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('gene_type')
          ->title('Sequence Type')
          ->table('tripal_megasearch_gene')
          ->column('type')
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      
      // Genome
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('gene_genome')
          ->title('Genome Name')
          ->table('tripal_megasearch_gene')
          ->column('analysis')
          ->condition("analysis_type='whole_genome'")
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('gene_glandmark')
          ->title('Chromosome/Scaffold')
          ->dependOnId('gene_genome')
          ->callback('tripal_megasearch_ajax_gene_populate_landmark')
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->newLine()
          );
      
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_gstart')
          ->title('Start')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_gstop')
          ->title('Stop')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('gene_group_genome')
          ->title('Genome')
          ->startWidget('gene_genome')
          ->endWidget('gene_gstop')
          );
      
      // Transcriptome
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('gene_transcriptome')
          ->title('Transcriptome Name')
          ->table('tripal_megasearch_gene')
          ->column('analysis')
          ->condition("analysis_type IN ('reftrans', 'transcriptome')")
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('gene_aligned_genome')
          ->title('Genome')
          ->table('tripal_megasearch_gene')
          ->column('genome')
          ->condition("analysis_type IN ('reftrans', 'transcriptome')")
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('gene_tlandmark')
          ->title('Chromosome/Scaffold')
          ->dependOnId('gene_transcriptome')
          ->callback('tripal_megasearch_ajax_gene_populate_landmark')
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->newLine()
          );
      
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_tstart')
          ->title('Start')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_tstop')
          ->title('Stop')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('gene_group_transcriptome')
          ->title('Transcriptome')
          ->startWidget('gene_transcriptome')
          ->endWidget('gene_tstop')
          );
      
      // Gene/Transcript Name
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_name')
          ->title('Name')
          ->labelWidth(150)
          ->fieldset('data_filters')
          );
      $form->addFile(
          Set::file()
          ->id('gene_file')
          ->title("File Upload")
          ->fieldset('data_filters')
          ->description("Provide germplasm names in a file. Separate each name by a new line.")
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('gene_group_name')
          ->title('Gene/Transcript name')
          ->startWidget('gene_name')
          ->endWidget('gene_file')
          );
      
      // Functional Annotation
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_all')
          ->title('All')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_blast')
          ->title('BLAST')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_kegg')
          ->title('KEGG')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_interpro')
          ->title('InterPro')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_go')
          ->title('GO Term')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_genbank')
          ->title('GenBank Keyword')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('gene_group_annotation')
          ->title('Functional Annotation')
          ->startWidget('gene_all')
          ->endWidget('gene_genbank')
          );
      
      // Select/unselect all fields
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('attribute_check_all')
          ->options(
              array(
                'select_all' => 'All Fields',
              )
              )
          ->ajax(
              array(
                'callback' => 'tripal_megasearch_ajax_toggle_field_selection',
                'wrapper' => 'chado_search-tripal_megasearch-checkboxes-attribute_checkboxes',
                'effect' => 'fade'
              )
              )
          ->defaultValue(array('select_all'))
          ->fieldset('data_attributes')
          );
      // Populate attribuet list
      $form->addCheckboxes(
          Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($fields)
          ->fieldset('data_attributes')
          ->defaultValue(array_keys($fields))
          );
    }
  }
  return $form;
}

function tripal_megasearch_ajax_gene_get_filters(&$form_state) {
  $where = array();
  $where [] = Sql::selectFilter('gene_genome', $form_state, 'analysis');
  $where [] = Sql::selectFilter('gene_glandmark', $form_state, 'landmark');
  $where [] = Sql::textFilter('gene_gstart', $form_state, 'fmin');
  $where [] = Sql::textFilter('gene_gstop', $form_state, 'fmax');
  $where [] = Sql::selectFilter('gene_transcriptome', $form_state, 'analysis');
  $where [] = Sql::selectFilter('gene_aligned_genome', $form_state, 'genome');
  $where [] = Sql::selectFilter('gene_tlandmark', $form_state, 'landmark');
  $where [] = Sql::textFilter('gene_tstart', $form_state, 'fmin');
  $where [] = Sql::textFilter('gene_tstop', $form_state, 'fmax');
  $where [] = Sql::textFilter('gene_name', $form_state, 'name');
  $where [] = Sql::selectFilter('gene_type', $form_state, 'type');
  $where [] = Sql::textFilterOnMultipleColumns('gene_all', $form_state, array('blast', 'kegg', 'interpro', 'go', 'genbank'));
  $where [] = Sql::textFilter('gene_blast', $form_state, 'blast');
  $where [] = Sql::textFilter('gene_kegg', $form_state, 'kegg');
  $where [] = Sql::textFilter('gene_interpro', $form_state, 'interpro');
  $where [] = Sql::textFilter('gene_go', $form_state, 'go');
  $where [] = Sql::textFilter('gene_genbank', $form_state, 'genbank');
  return $where;
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_gene_refresh_count ($form, &$form_state) {
  $where = tripal_megasearch_ajax_gene_get_filters($form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_gene_view_result ($form, &$form_state) {
  $where = tripal_megasearch_ajax_gene_get_filters($form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/*
  * Create download
  */
function tripal_megasearch_ajax_gene_create_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_gene_get_filters($form_state);
  $format = $form_state['triggering_element']['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}

/*
 * Create FASTA download
 */
function tripal_megasearch_ajax_gene_create_fasta_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_gene_get_filters($form_state);
  return tripal_megasearch_ajax_get_fasta_file ($form, $form_state, $where);
}

function tripal_megasearch_ajax_gene_populate_landmark($val) {
  $sql = "SELECT distinct landmark FROM {tripal_megasearch_gene} WHERE analysis = :analysis ORDER BY landmark";
  return chado_search_bind_dynamic_select(array(':analysis' => $val), 'landmark', $sql);
}