<?php

use ChadoSearch\Set;
use ChadoSearch\Sql;

function tripal_megasearch_marker_form ($form) {
  $form_state =& $form->form_state;
  
  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
  Set::dynamicFieldset()
  ->id('data_filters')
  ->title('Query')
  ->description('<i>Note: Some of the SNP positions in the genome are >1 when the alignment was done using flanking sequences.</i>')
  ->dependOnId('datatype');
  $setttings_attr =
  Set::dynamicFieldset()
  ->id('data_attributes')
  ->title('Downloadable Fields')
  ->dependOnId('datatype');
  if(!isset($form_state['values']['datatype']) || $form_state['values']['datatype'] == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }
  
  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);
  
  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);
  
  if(isset($form_state['values']['datatype'])) {
    $mview = $form_state['values']['datatype'];
    $fields = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
    if ($fields) {
      $form->addButton(
          Set::button()
          ->id('tsv-download')
          ->value('TSV')
          ->fieldset('data_attributes')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_marker_create_download',
              'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
              'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $form->addSubmit(
          Set::submit()
          ->value('CSV')
          ->fieldset('data_attributes')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_marker_create_download',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $default_rows = variable_get('tripal_megasearch_result_rows', '10');
      if ($default_rows > 0) {
        $form->addButton(
            Set::button()
            ->id('view_results')
            ->fieldset('data_attributes')
            ->value('View')
            ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_marker_view_result',
              'wrapper' => 'chado_search-tripal_megasearch-markup-results',
              'effect' => 'fade')
                )
            );
      }
      $form->addButton(
          Set::button()
          ->id('apply_filters')
          ->fieldset('data_filters')
          ->value('Refresh Count')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_marker_refresh_count',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          );
      $form->addButton(
          Set::button()
          ->id('clear-all-values')
          ->fieldset('data_filters')
          ->value('Clear')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_filter_clear_values',
              'wrapper' => 'chado_search-filter-tripal_megasearch-data_filters-field',
              'effect' => 'fade')
              )
          );
      
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('marker_type')
          ->title('Marker Type')
          ->table('tripal_megasearch_marker')
          ->column('marker_type')
          ->labelWidth(170)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('marker_name')
          ->title('Marker Name')
          ->labelWidth(170)
          ->fieldset('data_filters')
          );
      $form->addFile(
          Set::file()
          ->id('marker_file')
          ->title("File Upload")
          ->fieldset('data_filters')
          ->newLine()
          ->description("Provide marker names in a file. Separate each name by a new line.")
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('marker_snp_array')
          ->title('SNP Array Name')
          ->labelWidth(170)
          ->fieldset('data_filters')
          ->newLine()
          );
      
      // Organism
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('marker_mapped_organism')
          ->title('Mapped in Organism')
          ->table('tripal_megasearch_marker')
          ->column('mapped_organism')
          ->fieldset('data_filters')
          ->labelWidth(170)
          ->multiple(TRUE)
          ->searchBox(TRUE)
          ->cache(TRUE)
          ->newline()
          );
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('marker_organism')
          ->title('Developed in Organism')
          ->table('tripal_megasearch_marker')
          ->column('organism')
          ->fieldset('data_filters')
          ->multiple(TRUE)
          ->searchBox(TRUE)
          ->labelWidth(170)
          ->cache(TRUE)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('marker_group_org')
          ->title('Organism')
          ->startWidget('marker_mapped_organism')
          ->endWidget('marker_organism')
          );
      
      // Genome
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('marker_genome')
          ->title('Genome')
          ->table('tripal_megasearch_marker')
          ->column('genome')
          ->labelWidth(170)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('marker_glandmark')
          ->title('Chromosome/Scaffold')
          ->dependOnId('marker_genome')
          ->callback('tripal_megasearch_ajax_marker_populate_landmark')
          ->labelWidth(170)
          ->fieldset('data_filters')
          ->newLine()
          );      
      $form->addTextFilter(
          Set::textFilter()
          ->id('marker_gstart')
          ->title('Start')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(170)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('marker_gstop')
          ->title('Stop')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(170)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('marker_group_genome')
          ->title('Genome location')
          ->startWidget('marker_genome')
          ->endWidget('marker_gstop')
          );
      
      // Genetic location
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('marker_map')
          ->title('Map')
          ->column('map')
          ->table('tripal_megasearch_marker')
          ->cache(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(170)
          ->newLine()
          );
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('marker_lg')
          ->title('Linkage Group')
          ->dependOnId('marker_map')
          ->callback('tripal_megasearch_ajax_marker_populate_lg')
          ->fieldset('data_filters')
          ->labelWidth(170)
          ->newLine()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('marker_start')
          ->title('Start')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(170)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('marker_stop')
          ->title('Stop')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(170)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('marker_group_genetic_location')
          ->title('Genetic location')
          ->startWidget('marker_map')
          ->endWidget('marker_stop')
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('marker_citation')
          ->title('Citation')
          ->fieldset('data_filters')
          ->labelWidth(170)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('marker_group_pub')
          ->title('Publication')
          ->startWidget('marker_citation')
          ->endWidget('marker_citation')
          );

      $form->addTextFilter(
          Set::textFilter()
          ->id('marker_trait')
          ->title('Trait Name')
          ->fieldset('data_filters')
          ->labelWidth(170)
          ->newline()
          );
      
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('marker_sequence')
          ->options(array(
            'primers' => ' Has Marker Primer',
            'residues' => 'Has Marker Sequence'
          ))
          ->fieldset('data_filters')
      );

      // Select/unselect all fields
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('attribute_check_all')
          ->options(
            array(
              'select_all' => 'All Fields',
            )
          )
          ->ajax(
            array(
              'callback' => 'tripal_megasearch_ajax_toggle_field_selection',
              'wrapper' => 'chado_search-tripal_megasearch-checkboxes-attribute_checkboxes',
              'effect' => 'fade'
            )
          )
          ->defaultValue(array('select_all'))
          ->fieldset('data_attributes')
          );
      // Populate attribuet list
      $form->addCheckboxes(
          Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($fields)
          ->fieldset('data_attributes')
          ->defaultValue(array_keys($fields))
          );
    }
  }
  return $form;
}

function tripal_megasearch_ajax_marker_get_filters(&$form_state) {
  $where = array();
  $where [] = Sql::selectFilter('marker_type', $form_state, 'marker_type');
  //$where [] = Sql::textFilter('marker_name', $form_state, 'name');
  $where [] = Sql::textFilterOnMultipleColumns('marker_name', $form_state, array('uniquename', 'name', 'alias'));
  $where [] = Sql::file('marker_file', 'name');
  $where [] = Sql::textFilter('marker_snp_array', $form_state, 'snp_array');
  $where [] = Sql::selectFilter('marker_organism', $form_state, 'organism');
  $where [] = Sql::selectFilter('marker_mapped_organism', $form_state, 'mapped_organism');
  $where [] = Sql::selectFilter('marker_genome', $form_state, 'genome');
  $where [] = Sql::selectFilter('marker_glandmark', $form_state, 'landmark');
  $where [] = Sql::textFilter('marker_gstart', $form_state, 'fmin');
  $where [] = Sql::textFilter('marker_gstop', $form_state, 'fmax');
  $where [] = Sql::selectFilter('marker_map', $form_state, 'map');
  $where [] = Sql::selectFilter('marker_lg', $form_state, 'lg');
  $where [] = Sql::textFilter('marker_start', $form_state, 'start');
  $where [] = Sql::textFilter('marker_stop', $form_state, 'stop');
  $where [] = Sql::textFilter('marker_citation', $form_state, 'pubs');
  $where [] = Sql::textFilter('marker_trait', $form_state, 'trait');
  $where [] = Sql::checkBoxColumnNotEmpty('marker_sequence', $form_state);
  return $where;
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_marker_refresh_count ($form, &$form_state) {
  $where = tripal_megasearch_ajax_marker_get_filters($form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_marker_view_result ($form, &$form_state) {
  $where = tripal_megasearch_ajax_marker_get_filters($form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/*
  * Create download
  */
function tripal_megasearch_ajax_marker_create_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_marker_get_filters($form_state);
  $format = $form_state['triggering_element']['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}

function tripal_megasearch_ajax_marker_populate_landmark($val) {
  $sql = "SELECT distinct landmark FROM {tripal_megasearch_marker} WHERE genome = :genome ORDER BY landmark";
  return chado_search_bind_dynamic_select(array(':genome' => $val), 'landmark', $sql);
}

function tripal_megasearch_ajax_marker_populate_lg($val) {
  $sql = "SELECT distinct lg FROM {tripal_megasearch_marker} WHERE map = :map ORDER BY lg";
  return chado_search_bind_dynamic_select(array(':map' => $val), 'lg', $sql);
}
