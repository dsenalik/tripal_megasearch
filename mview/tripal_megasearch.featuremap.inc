<?php

function tripal_megasearch_create_featuremap_mview() {
  $view_name = 'tripal_megasearch_featuremap';
  chado_search_drop_mview($view_name);
  $schema = array(
    'table' => $view_name,
    'fields' => array(
      'featuremap_id' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
      'name' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'description' => array (
        'type' => 'text',
      ),
      'unittype' => array (
        'type' => 'varchar',
        'length' => '1024'
      ),
      'organism' => array(
        'type' => 'varchar',
        'length' => '510'
      ),
      'population' => array (
        'type' => 'text',
      ),
      'maternal_parent' => array (
        'type' => 'text',
      ),
      'paternal_parent' => array (
        'type' => 'text',
      ),
      'num_of_lg' => array (
        'type' => 'int',
      ),
      'num_of_loci' => array (
        'type' => 'int',
      ),
      'citation' => array (
        'type' => 'text',
      ),
      'trait' => array (
        'type' => 'text',
      ),
      'qtl' => array (
        'type' => 'text',
      ),
      'published_symbol' => array (
        'type' => 'text',
      ),
    )
  );
  $sql = "
    SELECT DISTINCT
      FM.featuremap_id,
      FM.name,
      FM.description,
      (SELECT name FROM cvterm WHERE cvterm_id = FM.unittype_id) AS unittype,
      organism,
      STK.uniquename AS population,
      MATP.uniquename AS maternal_parent,
      PATP.uniquename AS paternal_parent,
      (SELECT 
         count (distinct F.uniquename) 
       FROM featurepos FPos 
       INNER JOIN feature F ON F.feature_id = FPos.map_feature_id 
       WHERE F.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'linkage_group')
       AND Fpos.featuremap_id = FM.featuremap_id
      ) AS num_of_lg,
      (SELECT 
         count (F.uniquename) 
       FROM featurepos FPos 
       INNER JOIN feature F ON F.feature_id = FPos.feature_id 
       WHERE F.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'marker_locus' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
       AND  Fpos.featuremap_id = FM.featuremap_id
      ) AS num_of_loci,
      P.uniquename AS citation,
      (SELECT 
         string_agg(DISTINCT name, '::') 
       FROM featurepos PS 
       INNER JOIN feature QTL ON PS.feature_id = QTL.feature_id 
       AND QTL.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'QTL') 
       WHERE featuremap_id = FM.featuremap_id 
       GROUP BY featuremap_id
      ) AS trait,
      (SELECT 
         string_agg(DISTINCT uniquename, '::') 
       FROM featurepos PS 
       INNER JOIN feature QTL ON PS.feature_id = QTL.feature_id 
       AND QTL.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'QTL') 
       WHERE featuremap_id = FM.featuremap_id 
       GROUP BY featuremap_id
      ) AS qtl,
      (SELECT 
         string_agg(DISTINCT value, '::') 
       FROM featurepos PS 
       INNER JOIN feature QTL ON PS.feature_id = QTL.feature_id 
       AND QTL.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'QTL') 
       INNER JOIN featureprop FP ON QTL.feature_id = FP.feature_id
       AND FP.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'published_symbol')
       WHERE featuremap_id = FM.featuremap_id 
       GROUP BY featuremap_id
      ) AS published_symbol
    FROM featuremap FM
    LEFT JOIN (
      SELECT 
          FO.featuremap_id, 
          O.genus || ' ' || O.species AS organism,
          O.genus,
          O.species,
          O.organism_id
      FROM featuremap_organism FO 
      INNER JOIN organism O ON FO.organism_id = O.organism_id
    ) ORG ON ORG.featuremap_id = FM.featuremap_id
    LEFT JOIN (
      SELECT 
        S.stock_id, 
        S.uniquename, 
        FMS.featuremap_id 
      FROM stock S 
      INNER JOIN featuremap_stock FMS ON S.stock_id = FMS.stock_id
    ) STK ON STK.featuremap_id = FM.featuremap_id
    LEFT JOIN (
      SELECT 
        MAT.stock_id, 
        MAT.uniquename, 
        MSR.object_id 
      FROM stock MAT 
      INNER JOIN stock_relationship MSR ON MAT.stock_id = MSR.subject_id
      WHERE MSR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a_maternal_parent_of')
    ) MATP ON MATP.object_id = STK.stock_id
    LEFT JOIN (
      SELECT 
        PAT.stock_id, 
        PAT.uniquename, 
        PSR.object_id 
      FROM stock PAT 
      INNER JOIN stock_relationship PSR ON PAT.stock_id = PSR.subject_id
      WHERE PSR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a_paternal_parent_of')
    ) PATP ON PATP.object_id = STK.stock_id
    LEFT JOIN featuremap_pub FP ON FP.featuremap_id = FM.featuremap_id
    LEFT JOIN pub P ON P.pub_id = FP.pub_id      
  ";
  tripal_add_mview($view_name, 'tripal_megasearch', $schema, $sql, '', FALSE);
}