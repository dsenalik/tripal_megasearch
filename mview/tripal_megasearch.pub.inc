<?php

function tripal_megasearch_create_pub_mview() {
  $view_name = 'tripal_megasearch_pub';
  chado_search_drop_mview($view_name);
  $schema = array(
    'table' => $view_name,
    'fields' => array(
      'pub_id' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
      'title' => array(
        'type' => 'text'
      ),
      'volume' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'series_name' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'issue' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'pyear' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'pages' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'citation' => array(
        'type' => 'text'
      ),
      'type' => array (
        'type' => 'varchar',
        'length' => '1024'
      ),
      'authors' => array(
        'type' => 'text'
      ),
      'abstract' => array(
        'type' => 'text'
      ),
    )
  );
  $sql = "
    SELECT DISTINCT
      P.pub_id,
      title,
      volume,
      series_name,
      issue,
      pyear,
      pages,
      uniquename AS citation,
      (SELECT name FROM cvterm WHERE cvterm_id = P.type_id) AS type, 
      AUTH.value AS authors,
      ABS.value AS abstract
    FROM pub P
    LEFT JOIN pubprop ABS ON ABS.pub_id = P.pub_id AND ABS.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'Abstract')
    LEFT JOIN pubprop AUTH ON AUTH.pub_id = P.pub_id AND AUTH.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'Authors')
    WHERE title IS NOT NULL
  ";
  tripal_add_mview($view_name, 'tripal_megasearch', $schema, $sql, '', FALSE);
}