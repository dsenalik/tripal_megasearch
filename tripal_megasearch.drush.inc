<?php
/**
 * Registers a drush command and constructs the full help for that command
 *
 * @return
 *   array of command descriptions
 */
function tripal_megasearch_drush_command() {
  $items = array();
  $items['trpms-mview-recreate'] = array(
    'description' => dt('Drop and recreate a Tripal MegaSearch MView'),
    'options' => array(
      'name' => array(
        'description' => dt('The MView name to recreate.'),
      ),
    ),
    'examples' => array(
      'Standard example (reload settings)' => 'trpms-mview-recreate --name=tripal_megasearch_stock',
    ),
    'aliases' => array('trpms-mv'),
  );
  return $items;
}

function drush_tripal_megasearch_trpms_mview_recreate() {
  $mview = drush_get_option('name');
  if (!$mview) {
    print "Please specify the MView name by using --name option\n";
    return;
  }
  $settings = chado_search_get_mview('tripal_megasearch_data_definition', $mview);
  if ($settings) {
    print "Recreating MView '$mview'...\n";
    $func = str_replace('tripal_megasearch_', 'tripal_megasearch_create_', $mview) . '_mview';
    if (isset($settings['mview_file'])) {
      require_once($settings['mview_file']);
      print "Requiring custom mview file '" . $settings['mview_file'] . "'...\n";
    }
    else {
      module_load_include('inc', 'tripal_megasearch', 'mview/' . str_replace('tripal_megasearch_', 'tripal_megasearch.', $mview));
    }
    if (function_exists($func)) {
      $func();
    }
    else {
      print "Error: '$mview' is not a Tripal MegaSearch supporting MView.\n";
    }
  }
  else {
    print "Error: MView '$mview' not found. Please check your spelling.\n";
  }
}